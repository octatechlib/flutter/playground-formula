import 'package:flutter/material.dart';
import 'Events/AttributeChangeEvent.dart';

class CalculateForm extends StatefulWidget {
  const CalculateForm({super.key});

  final String title = 'Playground Calculator';

  @override
  CalculateFormState createState() => CalculateFormState();
}

class CalculateFormState extends State<CalculateForm> {
  final _formKey = GlobalKey<FormState>();

  double _calculatedoperandOneRatio = 0.00;
  double _calculatedoperandTwoRatio = 0.00;

  String _operandOneRatioToDisplay = "0.000";
  String _operandTwoRatioToDisplay = "0.000";

  late TextEditingController _operandOneField;
  late TextEditingController _operandTwoField;
  late TextEditingController _operandOneMultiplierField;
  late TextEditingController _operandOneRatioField;
  late TextEditingController _operandTwoMultiplierField;
  late TextEditingController _operandTwoRatioField;

  @override
  void initState() {
    super.initState();

    _operandOneField = TextEditingController();
    _operandOneField.text = '';
    _operandOneField.addListener(_onChange);

    _operandOneField.addListener(_triggeroperandOneRatioUpdate);
    _operandOneField.addListener(_triggeroperandTwoRatioUpdate);

    _operandTwoField = TextEditingController();
    _operandTwoField.text = '';
    _operandTwoField.addListener(_onChange);

    _operandTwoField.addListener(_triggeroperandOneRatioUpdate);
    _operandTwoField.addListener(_triggeroperandTwoRatioUpdate);

    _operandOneMultiplierField = TextEditingController();
    _operandOneMultiplierField.text = '';
    _operandOneMultiplierField.addListener(_onChange);

    _operandOneMultiplierField.addListener(_triggeroperandOneRatioUpdate);
    _operandOneMultiplierField.addListener(_triggeroperandTwoRatioUpdate);

    _operandOneRatioField = TextEditingController();
    _operandOneRatioField.text = '1.36';
    _operandOneRatioField.addListener(_onChange);

    _operandOneRatioField.addListener(_triggeroperandOneRatioUpdate);

    _operandTwoMultiplierField = TextEditingController();
    _operandTwoMultiplierField.text = '';
    _operandTwoMultiplierField.addListener(_onChange);

    _operandTwoMultiplierField.addListener(_triggeroperandTwoRatioUpdate);

    _operandTwoRatioField = TextEditingController();
    _operandTwoRatioField.text = '1.36';
    _operandTwoRatioField.addListener(_onChange);

    _operandTwoRatioField.addListener(_triggeroperandOneRatioUpdate);
    _operandTwoRatioField.addListener(_triggeroperandTwoRatioUpdate);

    // end
  }

  @override
  void dispose() {
    _operandOneField.dispose();
    _operandTwoField.dispose();
    _operandOneMultiplierField.dispose();
    _operandOneRatioField.dispose();
    _operandTwoMultiplierField.dispose();
    _operandTwoRatioField.dispose();

    super.dispose();
  }

  void _onChange() {
    final attributes = <String, String>{
      'operandOne': _operandOneField.text.toString(),
      'operandTwo': _operandTwoField.text.toString(),
      'operandOneMultiplier': _operandOneMultiplierField.text.toString(),
      'operandOneRatio': _operandOneRatioField.text.toString(),
      'operandTwoMultiplier': _operandTwoMultiplierField.text.toString(),
      'operandTwoRatio': _operandTwoRatioField.text.toString()
    };

    final eventParams = new Map<String, String>();
    eventParams.addEntries(attributes.entries);

    var event = AttributeChangeEvent(eventParams);

    this._calculatedoperandOneRatio = event.getOperandOneRatio();
    this._calculatedoperandTwoRatio = event.getOperandTwoRatio();
  }

  void _triggeroperandOneRatioUpdate() {
    setState(() {
      _operandOneRatioToDisplay =
          _calculatedoperandOneRatio.toStringAsPrecision(5);
    });
  }

  void _triggeroperandTwoRatioUpdate() {
    setState(() {
      _operandTwoRatioToDisplay = _calculatedoperandTwoRatio.toStringAsPrecision(5);
    });
  }

  void _exchangePercentToRatio() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 32.0),
                child: TextFormField(
                  controller: _operandOneField,
                  decoration: const InputDecoration(
                      isDense: true,
                      prefixIconConstraints:
                          BoxConstraints(minWidth: 50, minHeight: 50),
                      labelText: 'Operand one',
                      hintText: 'e.g. 1,1234'),
                  validator: (v) {
                    if (v.toString().isEmpty) {
                      return 'Please enter operand one';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 32.0),
                child: TextFormField(
                  controller: _operandTwoField,
                  decoration: const InputDecoration(
                      isDense: true,
                      prefixIconConstraints:
                          BoxConstraints(minWidth: 50, minHeight: 50),
                      labelText: 'Operand Two',
                      hintText: 'e.g. 12,00'),
                  validator: (v) {
                    if (v.toString().isEmpty) {
                      return 'Please enter operand two';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 32.0),
                child: TextFormField(
                  controller: _operandOneMultiplierField,
                  decoration: const InputDecoration(
                      isDense: true,
                      //prefixIcon: Text("\% "),
                      prefixIconConstraints:
                          BoxConstraints(minWidth: 50, minHeight: 50),
                      labelText: 'Operand one multiplier',
                      hintText: 'e.g. 150%'),
                  validator: (v) {
                    if (v.toString().isEmpty) {
                      return 'Please enter multiplier for operand one';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 32.0),
                child: TextFormField(
                  controller: _operandOneRatioField,
                  decoration: const InputDecoration(
                      isDense: true,
                      prefixIconConstraints:
                          BoxConstraints(minWidth: 50, minHeight: 50),
                      labelText: 'Operand one ratio',
                      hintText: 'e.g. 1,36'),
                  validator: (v) {
                    if (v.toString().isEmpty) {
                      return 'Please enter operand one ratio';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 32.0),
                child: TextFormField(
                  controller: _operandTwoMultiplierField,
                  decoration: const InputDecoration(
                      isDense: true,
                      prefixIconConstraints:
                          BoxConstraints(minWidth: 50, minHeight: 50),
                      labelText: 'Operand two multiplier',
                      hintText: 'e.g. 125%'),
                  validator: (v) {
                    if (v.toString().isEmpty) {
                      return 'Please enter multiplier for operand two';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 32.0),
                child: TextFormField(
                  controller: _operandTwoRatioField,
                  decoration: const InputDecoration(
                      isDense: true,
                      prefixIconConstraints:
                          BoxConstraints(minWidth: 50, minHeight: 50),
                      labelText: 'Operand two ratio',
                      hintText: 'e.g. 1,36'),
                  validator: (v) {
                    if (v.toString().isEmpty) {
                      return 'Please enter operand two ratio';
                    }
                    return null;
                  },
                ),
              ),

              // space and button
              const SizedBox(
                height: 40,
              ),
              const Text(
                'Ratio:',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                ),
              ),
              Text(_operandOneRatioToDisplay),
              const Text(
                'Ratio:',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                ),
              ),
              Text(_operandTwoRatioToDisplay),
            ],
          ),
        ),
      ),
    );
  }
}

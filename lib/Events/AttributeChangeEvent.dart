import 'package:my_app/Calculator/ShiftCalculator.dart';
import 'package:my_app/Calculator/DetailedCalculator.dart';

class AttributeChangeEvent {
  Map<String, String> params;

  double operandOneRatio = 0.00;
  double operandTwoRatio = 0.00;

  AttributeChangeEvent(this.params) {
    // TODO: not here

    var operandOne = params['operandOne'].toString();
    var operandTwo = params['operandTwo'].toString();

    var operandOneMultiplier = params['operandOneMultiplier'].toString();
    var operandOneRatio = params['operandOneRatio'].toString();

    var operandTwoMultiplier = params['operandTwoMultiplier'].toString();
    var operandTwoRatio = params['operandTwoRatio'].toString();

    if (operandOne.isEmpty) operandOne = "0.000";
    if (operandTwo.isEmpty) operandTwo = "0.000";

    if (operandOneMultiplier.isEmpty) operandOneMultiplier = "0%";
    if (operandOneRatio.isEmpty) operandOneRatio = "0.000";
    if (operandTwoMultiplier.isEmpty) operandTwoMultiplier = "0%";
    if (operandTwoRatio.isEmpty) operandTwoRatio = "0.000";

    operandOne = operandOne.replaceFirst(RegExp(','), '.');
    double doperandOne = double.parse(operandOne.toString());
    assert(doperandOne is double);

    operandTwo = operandTwo.replaceFirst(RegExp(','), '.');
    double doperandTwo = double.parse(operandTwo.toString());
    assert(doperandTwo is double);

    // %
    operandOneMultiplier = operandOneMultiplier.replaceFirst(RegExp(','), '.');
    operandOneMultiplier = operandOneMultiplier.replaceFirst(RegExp('%'), '');

    double dOperandOneRatio = double.parse(operandOneMultiplier.toString());
    if (dOperandOneRatio != 0) {
      dOperandOneRatio = dOperandOneRatio / 100;
    }
    assert(dOperandOneRatio is double);

    // %
    operandTwoMultiplier = operandTwoMultiplier.replaceFirst(RegExp(','), '.');
    operandTwoMultiplier = operandTwoMultiplier.replaceFirst(RegExp('%'), '');

    double dOperandTwoRatio = double.parse(operandTwoMultiplier.toString()) / 100;
    if (dOperandTwoRatio != 0) {
      dOperandTwoRatio = dOperandTwoRatio / 100;
    }
    assert(dOperandTwoRatio is double);

    operandOneRatio =
        operandOneRatio.replaceFirst(RegExp(','), '.');
    double doperandOneRatio =
        double.parse(operandOneRatio.toString());
    assert(doperandOneRatio is double);

    operandTwoRatio = operandTwoRatio.replaceFirst(RegExp(','), '.');

    double doperandTwoRatio =
        double.parse(operandTwoRatio.toString());
    assert(doperandTwoRatio is double);

    // TODO: check if none of them in NaN
    var calculatedOperandOneRatio = new DetailedCalculator(
        doperandOne, doperandTwo, dOperandOneRatio, doperandOneRatio);
    var calculatedOperandTwoRatio = new ExtraCalculator(
        doperandOne, doperandTwo, dOperandTwoRatio, doperandTwoRatio);

    this.operandOneRatio = calculatedOperandOneRatio.calculate();
    this.operandTwoRatio = calculatedoperandTwoRatio.calculate();
  }

  double getOperandOneRatio() {
    return this.operandOneRatio;
  }

  double getOperandTwoRatio() {
    return this.operandTwoRatio;
  }
}

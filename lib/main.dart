import 'package:flutter/material.dart';
import 'app.dart';
import 'dart:io';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    //setWindowTitle('Flutter Demo');
    //setWindowMinSize(const Size(400, 300));
    //setWindowMaxSize(Size.infinite);
  }

  runApp(const App());
}

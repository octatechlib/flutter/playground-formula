import 'package:my_app/Calculator/BaseCalculator.dart';

class DetailedCalculator extends BaseCalculator {
  DetailedCalculator(super.operandOne, super.operandTwo, super.operandThree, super.ratio);
}

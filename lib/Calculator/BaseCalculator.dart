class BaseCalculator {
  double operandOne;
  double operandTwo;
  double operandThree;

  BaseCalculator(this.operandOne, this.operandTwo, this.operandThree, this.ratio) {
    // ..
  }

  double calculate() {
    if (operandThree == 0) return 0;
    return ((operandOne + operandTwo) / operandThree) * ratio;
  }
}

# Playground formula

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Build project

- [ ] [Read more here](https://docs.flutter.dev/reference/flutter-cli)

```
flutter run
```

## Contributing
...

## Authors and acknowledgment
...

## License
GPL

## Project status
Sandbox
